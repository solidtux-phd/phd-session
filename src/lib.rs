use std::time::{SystemTime, UNIX_EPOCH};

use hdf5::{types::VarLenUnicode, H5Type};

#[derive(H5Type, Debug, Clone)]
#[repr(C)]
pub struct Session {
    pub software: Software,
    pub run: Run,
}

#[derive(H5Type, Debug, Clone)]
#[repr(C)]
pub struct Software {
    pub name: VarLenUnicode,
    pub version: VarLenUnicode,
    pub compile_time: VarLenUnicode,
}

#[macro_export]
macro_rules! vergen_session {
    () => {{
        #[cfg(feature = "git")]
        const fn version() -> &'static str {
            env!("VERGEN_GIT_SEMVER")
        }

        #[cfg(not(feature = "git"))]
        const fn version() -> &'static str {
            env!("VERGEN_BUILD_SEMVER")
        }

        let version = version().to_string().parse().unwrap();
        ::phd_session::Session {
            software: ::phd_session::Software {
                name: env!("CARGO_PKG_NAME").to_string().parse().unwrap(),
                version,
                compile_time: env!("VERGEN_BUILD_TIMESTAMP").to_string().parse().unwrap(),
            },
            run: ::phd_session::Run::default(),
        }
    }};
}

#[derive(H5Type, Debug, Clone)]
#[repr(C)]
pub struct Run {
    pub date: u64,
    pub host: VarLenUnicode,
}

impl Default for Run {
    fn default() -> Self {
        Run {
            date: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_secs(),
            host: ::hostname::get()
                .unwrap()
                .into_string()
                .unwrap()
                .parse()
                .unwrap(),
        }
    }
}
